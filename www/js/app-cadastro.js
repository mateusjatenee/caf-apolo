$('form').submit(function(){
    var landmarkID = $(this).parent().attr('data-landmark-id');
    var postData = $(this).serialize();

    $.ajax({
        type: 'POST',
        data: postData+'&amp;lid='+landmarkID,
        url: 'http://apolorestaurante.com/app/model/cadastro.php',
        success: function(data){
            console.log(data);
            alert('Conta criada com sucesso');
        },
        error: function(){
            console.log(data);
            alert('Houve um erro ao criar sua conta');
        }
    });

    return false;
});